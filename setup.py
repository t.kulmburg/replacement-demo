setup(
    extras_require=dict(
        celery=[
            'celery>=3.1.13.0',
        ]
    ),
    install_requires=[
        'celery>=3.1.13.0',
    ]
)

