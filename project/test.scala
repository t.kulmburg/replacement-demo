import sbt._

object Dependencies {
  
  val abcVersion = "2.6.19"

  lazy val abc = "com.typesafe.akka" %% "akka-actor-typed" % abcVersion
}
