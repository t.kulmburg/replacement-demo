# README

## Replace working:

- **ansible** *tasks/main.yml*
- **ansible-galaxy** *requirements.yaml*
- **azure-pipelines** (container image) *azure-pipeline.yml*
- **batect** (image) *batect.yml*
- **bitbucket-pipelines** *bitbucket-pipelines.yml*
- **buildkite** (not for docker images inside) *buildkite.yml*
- **bundler** *Gemfile*
- **cake** *build.cake*
- **cargo** *Cargo.toml*
- **circleci** *.circleci/config.yml*
- **cloudbuild** (not for 'images') *cloudbuild.yml*
- **cocoapods** *Podfile*
- **composer** *composer.json*
- **deps-edn** *deps.edn*
- **docker-compose** *docker-compose.yml*
- **dockerfile** *Dockerfile*
- **droneci** *.drone.yml*
- **gitlabci** *.gitlab-ci.yml*
- **helm-values** *values.yaml*
- **jenkins** *plugins.txt*
- **meteor** *package.js*
- **mix** *mix.exs*
- **nuget** *dotnet-tools.json*
- **pre-commit** *pre-commit-config.yaml*
- **terraform** *test.tf*
- **kubernetes** *kubernetes.yaml*
- **setup-cfg** *setup.cfg*
- **terragrunt** *terragrunt.hcl*
- **pip_requirements** *requirements.txt*
- **pipenv** *Pipfile*
- **poetry** *pyproject.toml*
- **pip-compile** *pipcompile.txt*
- **pip_setup** *setup.py*

#### Contain only version (working)

- **nvm** *.nvmrc*
- **travis** *.travis.yml*
- **nodenv** *.node-version*
- **pyenv** *.python-version*
- **terraform-version** *.terraform-version*
- **ruby-version** *.ruby.version*
- **terragrunt-version** *.terragrun-version*
- **gradle-wrapper** *gradle/wrapper/gradle-wrapper.properties*

## Feature Missing:

- **cdnurl** (needs newPackageName, only depName not enough)
- **html** (needs newPackageName, only depName not enough)
- **helmfile** *helmfile.yaml* (if different registry)
- **helmsman** *helmsman.yaml* (if different registry)
- **gitlabci-include** (no update for 'file' possible)('project' and 'ref' working)
- **kustomize** *kustomization.yaml* (replacing subdirectory not possible)

#### Need URL replacement

- **argocd** *argocd.yml*
- **helmv3** *Chart.yaml* 
- **helm-requirements** *requirements.yaml*

#### Only version not name replaced

- **leiningen** *project.clj* (depName: org.clojure:clojure while file contains org.clojure/clojure, depName not found in file)
- **pub** *pubspec.yaml*
- **jsonnet-bundler** *jsonnetfile.json*


#### Dep found but no 'updates'

- **github-action** *action.yml*
- **swift** *Package.swift*
- **sbt** *project/test.scala* *test.sbt*
- **batect-wrapper** *batect*




