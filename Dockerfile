FROM ubuntu:16.04@sha256:a3785f78ab8547ae2710c89e627783cfa7ee7824d3468cae6835c9f4eae23ff7

FROM openjdk:8u342-jre

FROM openjdk:17-slim-buster

FROM gcr.io/google-containers/alpine-with-bash:1.0

FROM registry.hub.docker.com/library/ubuntu:16.04
