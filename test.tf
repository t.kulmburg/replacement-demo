resource "docker_image" "ubuntu" {
  name = "ubuntu:16.04"
}

resource "kubernetes_test" "demo" {
  metadata {}
  spec {
    job_template {
      metadata {}
      spec {
        template {
          metadata {}
          spec {
            container {
              name    = "ubuntu"
              image   = "ubuntu:16.04"
            }
          }
        }
      }
    }
    schedule = ""
  }
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "3.14.2"
}


