import PackageDescription

let package = Package(
    name: "testPackage",
    platforms: [.macOS(.v10_16)],
    products: [
        .executable(
            // MARK: AvitoRunner
            name: "test",
            targets: [
                "test"
            ]
        )
    ],

    dependencies : [
        .package(url: "https://github.com/ReactiveX/RxSwift.git", .exact("6.5.0"))
    ],

    targets: [
        .target(
            name: "TestTestr",
            dependencies: [
                .product(name: "GithubPackage", package: "RxSwift")
            ]
        )
    ]
)

